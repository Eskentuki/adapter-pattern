package adapter;

public class Gamepad {
    public void connection(){
        System.out.println("The gamepad is connecting ...");
    }
    public void ready(){
        System.out.println("The gamepad successfully connected. You can use on the phone");
    }
}
