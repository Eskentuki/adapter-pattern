package adapter;

public class AdapterTypeC implements TypeC {

    private Gamepad gamepad;

    public AdapterTypeC(Gamepad PS4gamepad){
        this.gamepad = PS4gamepad;
    }

    public void connectwithTypeC() {
        this.gamepad.connection();
        this.gamepad.ready();
    }
}
